----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/20/2017 06:26:42 AM
-- Design Name: 
-- Module Name: tester - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity serial is
Port(
            tx:out std_logic;
            rx:in std_logic;
            clk : in STD_LOGIC;
         start:in std_logic;
          
          tx_data:in std_logic_vector(7 downto 0);
          tx_data2:in std_logic_vector(15 downto 0));
end serial;

architecture Behavioral of serial is
component bauder is
Port(clk:in std_logic;
baud_rate:out std_logic);
end component;
component TM is
Port(
clk:in std_logic;
tx_data:in std_logic_vector(7 downto 0);

tx_en:in std_logic;
rst:in std_logic;
baud_en:in std_logic;
tx:out std_logic;
tx_ready:out std_logic);

end component;
component debouncer is
Port(
clk:in std_logic;
trigger:in std_logic;
debounced:out std_logic);
end component;
signal baud_en:std_logic;


signal tx_en:std_logic;
signal tx_rdy:std_logic;
signal rst:std_logic;
signal count:std_logic;
signal q:std_logic;
signal tx1:std_logic;
signal start1:std_logic;
signal ascii_code:std_logic_vector(7 downto 0);
signal full_data:std_logic_vector(15 downto 0);
signal counter:std_logic_vector(2 downto 0):="100";
signal tmp:std_logic_vector(7 downto 0);
signal previous:std_logic;
begin

c3:debouncer port map(
clk,
start,
start1);
C1:bauder port map (
clk,
baud_en);
C2:TM port map (
clk,
ascii_code,
tx_en,
rst,
baud_en,
tx,
tx_rdy);
with counter select tmp<="0000"&tx_data2(3 downto 0) when "000",
                           "0000"& tx_data2(11 downto 8) when "001",
                           "0000"& tx_data2(7 downto 4) when "010",
                           "0000"& tx_data2(3 downto 0) when "011",
                          x"00" when others;
                        
ascii_code<=tmp+x"30" when tmp < 10 else tmp+x"41"-10;
--tmp<=("00000"&counter);
--ascii_code<=tmp +x"30";
process(clk)
    begin
        if(rising_edge(clk)) then
           
            if(baud_en='1') then
                        q<='0';
                        else
                        --and counter /="11"
                         if(   start='1' and counter>3 ) then
                                counter<="000";
                                
                                 q<='1';
                                else
                              if(tx_rdy='1') then
                                    if(counter="100") then
                                       
                                        q<='0';
                                     else
                                        counter<=counter+'1';
                                        q<='1';
                                     end if;
                
                                end if;
                        end if;
            
             
           
              end if;
           previous<=start; 
        end if;
     
    end process;

tx_en<=q;

rst<='0';
end Behavioral;
