----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/05/2017 09:09:13 AM
-- Design Name: 
-- Module Name: decoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder is
Port(
clk:in std_logic;
instr:in std_logic_vector(2 downto 0);
reg_dest:inout std_logic;
ext_op:inout std_logic;
alu_src:inout std_logic;
branch:inout std_logic;
jump:inout std_logic;
alu_op:inout std_logic_vector(2 downto 0);
mem_write:inout std_logic;
mem_to_reg:inout std_logic;
reg_write:inout std_logic
);


end decoder;

architecture Behavioral of decoder is
type pipe is array(0 to 20) of std_logic;
signal delay0:pipe;
signal delay1:pipe;
signal delay2:pipe;
signal delay3:pipe;
signal alu_op_delay0:std_logic_vector(2 downto 0);
signal alu_op_delay1:std_logic_vector(2 downto 0);

begin
jump<='1' when instr="111" else '0' ;

  delay0(3)<='1' when instr="100" else '0';
    branch<=delay2(3);

with instr SELect
   alu_op_delay0<=
   "010" when "110",
   "011" when "101",
   "000" when ("010" or "001" or "011" or "111"),
   "000" when others;
  
   
  delay0(2)<='1' when instr="011" else '0';
  mem_write<=delay2(2);
 delay0(4)<='1' when instr="000" else '0';
  alu_src<=delay1(4);
  
  delay0(1)<='1' when instr="000"  or instr="110" or instr="101" or instr="001" or instr="010" else '0' ;
  reg_write<=delay3(1);
  
    
    reg_dest<='1'when instr="000" else '0';
  --when do i init sign extension ?
    ext_op<='1' when (instr="001" or instr="100" or instr="111") else '0';    

    mem_to_reg<=delay3(0);
     delay0(0)<='1' when instr="010" else '0';
     alu_op<=alu_op_delay1;
    process(clk)
    begin
        if(rising_edge(clk)) then
            ----MEM/WB
            delay3(0)<=delay2(0);
            delay3(1)<=delay2(1);
            
            ---ex/mem
            delay2(0)<=delay1(0);
            delay2(1)<=delay1(1);
            delay2(2)<=delay1(2);
            delay2(3)<=delay1(3);
            ------idex
            alu_op_delay1<=alu_op_delay0;
                delay1(0)<=delay0(0);
                delay1(1)<=delay0(1);
                delay1(2)<=delay0(2);
                delay1(3)<=delay0(3);
                delay1(4)<=delay0(4);
               
                
            end if;
    end process;
            
    
            

end Behavioral;
