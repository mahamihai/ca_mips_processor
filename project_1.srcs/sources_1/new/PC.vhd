----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/30/2017 10:15:54 AM
-- Design Name: 
-- Module Name: PC - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PC is
--  Port ( );
Port(
clk:in std_logic;
branch:in std_logic;
jump:in std_logic;
branch_a:in std_logic_vector(15 downto 0);
jump_a:in std_logic_vector(15 downto 0);
old_a:in std_logic_vector(15 downto 0);
new_a:out std_logic_vector(15 downto 0);
instr:out std_logic_vector(15 downto 0);
pc_1:out std_logic_vector(15 downto 0)
);
end PC;

architecture Behavioral of PC is
type mem is array (0 to 255) of std_logic_vector(15 downto 0);
signal rom:mem:=(x"0DB7",
x"2204",
x"2101",
x"0000",
x"0000",
x"0000",
x"4500",
x"0000",
x"0000",
x"0000",
x"4181",
x"0000",
x"0000",
x"0000",
x"0990",
x"0000",
x"0000",
x"0000",
x"0830",
x"0000",
x"0000",
x"0000",
x"0420",
x"0000",
x"0000",
x"0000",
x"0470",
x"0000",
x"0000",
x"0000",
x"E00E",
others =>x"1234");
signal a_selection:std_logic_vector(1 downto 0);
signal aux_a:std_logic_vector(15 downto 0);

begin



aux_a<=old_a+'1';
pc_1<=aux_a;
a_selection<=branch&jump;
with a_selection select
     new_a<=branch_a when "10",
    jump_a when "01",
   aux_a when others;
instr<=rom(conv_integer(old_a));
   



end Behavioral;
