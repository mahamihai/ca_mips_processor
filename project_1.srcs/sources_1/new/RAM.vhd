----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/19/2017 07:59:10 PM
-- Design Name: 
-- Module Name: RAM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RAM is
--  Port ( );
Port(
clk:in std_logic;
ram_enable:in std_logic;

ram_data:in std_logic_vector(15 downto 0);
num:in std_logic_vector(15 downto 0 );
ram_out:out std_logic_vector(15 downto 0)

);

end RAM;

architecture Behavioral of RAM is
type mem is array (0 to 255) of std_logic_vector(15 downto 0);
signal ram:mem:=(x"0001",x"0001",others =>x"0007");

begin
process (clk)
begin
    if(rising_edge(clk)) then
        if(ram_enable='1') then
           ram(conv_integer(num(7 downto 0)))<= ram_data;
            end if;
        ram_out<=ram(conv_integer(num(7 downto 0))) ;
           
      end if;
end process;


end Behavioral;
