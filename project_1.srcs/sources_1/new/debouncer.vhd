----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/19/2017 07:44:37 PM
-- Design Name: 
-- Module Name: debouncer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer is
Port(
clk:in std_logic;
trigger:in std_logic;
debounced:out std_logic);
end debouncer;

architecture Behavioral of debouncer is
signal debounce:std_logic_vector(15 downto 0):="0000000000000000";
signal q1:std_logic;
signal q2:std_logic;
signal q0:std_logic;
begin
 process(clk)
begin
if(rising_edge(clk))
then
   debounce<=debounce+'1';
   if(debounce=x"FFFF")
       then
       
       q0<=trigger;
     
      end if;
      q1<=q0;
      q2<=q1;
      
      debounced<=q1 and not q2;
      

                
           
    
end if;
end process;


end Behavioral;
