----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2017 07:58:47 AM
-- Design Name: 
-- Module Name: rx - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rx is
    Port(
    clk:in std_logic;
    rx_rdy:out std_logic;
    rx_data:out std_logic_vector(7 downto 0);
    rst:in std_logic;
    rx:in std_logic);
end rx;

architecture Behavioral of rx is

type state_type is (idle,start,bits,stop,waits);
signal state:state_type;
signal baud_count:std_logic_vector(7 downto 0);
signal bit_cnt:std_logic_vector(7 downto 0);
signal baud_en:std_logic;
signal word:std_logic_vector(7 downto 0);
component bauder2 is
Port(clk:in std_logic;
baud_rate:out std_logic);
end component;
begin
C1:bauder2 port map(
clk,
baud_en
);
rx_data<=word;
process(clk)
begin
    
    if(rst='1') then
        state<=idle;
     else
     if(rising_edge(clk)) then 
        if(baud_en='1') then  
            case state is
                 when idle=> if(rx='1') then 
                                                state<=start ;
                                                baud_count<=x"00";
                                        end if;
                 when start=>
                               
                                    
                                if(rx='1') then
                                        state<=idle;
                                    else
                                    if(baud_count=7 and rx='0') then 
                                                baud_count<=x"00";
                                             state<=bits;
                                        else
                                                state<=start;
                                        
                                         end if;
                              end if;
                 
                when bits=> 
                            if(bit_cnt=7 and baud_count=15) then
                                state<=stop;
                                baud_count<=x"00";
                                else
                                  baud_count<=baud_count+'1';
                                    state<=bits;
                                end if;
               when stop=>if(baud_count=15) then
                                    state<=waits;
                                    baud_count<=x"00";
                               else
                                 baud_count<=baud_count+'1';
                                     state<=stop;
                                     
                               end if;
              when waits=> if(baud_count=7) then
                                state<=idle;
                            else
                              baud_count<=baud_count+'1';
                                state<=waits;
                            end if;
             end case;
             end if;
   end if;
 end if;
 end process;
 process(state)
 begin
    case state is
        when idle=>rx_rdy<='0';
                    
        when start=>rx_rdy<='0';
                 
                     
                  
        when bits=>rx_rdy<='0';
                   
                   word(conv_integer(bit_cnt))<=rx;
                  
        when stop=>rx_rdy<='0';
        when waits=>rx_rdy<='1';
     end case;
 end process;


end Behavioral;
