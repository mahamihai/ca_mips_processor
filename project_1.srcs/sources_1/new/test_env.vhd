----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/05/2017 08:28:52 AM
-- Design Name: 
-- Module Name: test_env - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all; 
  
use ieee.numeric_std.all; 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test_env is
Port(
    clk:in std_logic;
	reg_write:in std_logic;
	instr:in std_logic_vector(15 downto 0 );
	reg_dest:in std_logic;
	ext_op:in std_logic;
	write_data:in std_logic_vector(15 downto 0);
	rd1:out std_logic_vector(15 downto 0);
	rd2:out std_logic_vector(15 downto 0);
	ext_imm:out std_logic_vector(15 downto 0 );
	funct:out std_logic_vector(2 downto 0);
	sa:out std_logic
	);
end test_env;

architecture Behavioral of test_env is
signal write_add:std_logic_vector(2 downto 0);
signal extended:std_logic_vector(15 downto 0);
component reg_file is
port (
clk : in std_logic;
ra1 : in std_logic_vector (2 downto 0);
ra2 : in std_logic_vector (2 downto 0);
wa : in std_logic_vector (2 downto 0);
wd : in std_logic_vector (15 downto 0);
wen : in std_logic;
rd1 : out std_logic_vector (15 downto 0);
rd2 : out std_logic_vector (15 downto 0)

);
end component;
signal wd0:std_logic_vector(2 downto 0);
signal wd1:std_logic_vector(2 downto 0);
signal wd2:std_logic_vector(2 downto 0);
signal wd3:std_logic_vector(2 downto 0);

begin
process(clk)
begin
    if(rising_edge(clk)) then
        wd3<=wd2;
        wd2<=wd1;
        wd1<=wd0;
        
        end if;
     end process;

C1:reg_file port map(
clk,
instr(12 downto 10),
instr(9 downto 7),
write_add,
write_data,
reg_write,
rd1,
rd2);

write_add<=wd3;
wd0<=instr(9 downto 7) when reg_dest='0' else
             instr(6 downto 4) when reg_dest='1';
             
funct<=instr(2 downto 0);
sa<=instr(3);
extended(6 downto 0)<=instr(6 downto 0);
extended(15 downto 7)<=(others=>instr(6)); 
ext_imm<="000000000" &instr(6 downto 0) when ext_op='0' else extended ;
            
              
                






end Behavioral;
