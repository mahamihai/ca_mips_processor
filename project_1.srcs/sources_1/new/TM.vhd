----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/20/2017 05:39:42 AM
-- Design Name: 
-- Module Name: TM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TM is
Port(
clk:in std_logic;
tx_data:in std_logic_vector(7 downto 0);
tx_en:in std_logic;
rst:in std_logic;
baud_en:in std_logic;
tx:out std_logic;
tx_ready:out std_logic);

end TM;

architecture Behavioral of TM is



signal state:std_logic_vector(1 downto 0):="00";
signal count:std_logic_vector(2 downto 0);
signal data:std_logic_vector(7 downto 0);
begin

process(clk,rst)
begin
    if(rst='1') then
        state<="00";
        
     else
        if(rising_edge(clk)) then
            if(baud_en='1') then
                    case(state) is
                    when "00" =>
                               if(tx_en='1') then
                                   state<="01";
                                   data<=tx_data;
                               else
                                   state<="00";
                                   end if;
                    when "01"=> 
                                state<="10";
                                count<="000";
                    when "10"=>                  
                                 if(count=7)
                                   then
                                   count<="000";
                                          state<="11";
                                    else
                                    count<=count+'1';
                                        state<="10";
                                   end if;     
                    when "11"=>
                                
                                state<="00";
                    end case;
          end if;  
        end if;
end if;
end process;
process(state)
begin
    case (state) is
    when "00"=> tx<='1';
                 tx_ready<='1';
    when "01"=>tx<='0';
              tx_ready<='0';
              --count<="000"; 
    when "10"=>       
                tx<=data(conv_integer(count));
                 tx_ready<='0';
               
                
    when "11"=>
      --count<="000";
        tx<='1';
         tx_ready<='0';
    end case;
    end process;

end Behavioral;
