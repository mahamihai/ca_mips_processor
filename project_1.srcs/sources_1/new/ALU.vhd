----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/19/2017 09:09:49 PM
-- Design Name: 
-- Module Name: ALU - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU is
--  Port ( );
Port(

a:in std_logic_vector(15 downto 0);
b:in std_logic_vector(15 downto 0);
operation:in std_logic_vector(2 downto 0);
zf:out std_logic;

result:inout std_logic_vector(15 downto 0)
);
end ALU;

architecture Behavioral of ALU is
signal r:std_logic_vector(15 downto 0);
signal l:std_logic_vector(15 downto 0);

begin
zf<='1' when conv_integer(result)=0 else '0';


l<=a(14 downto 0) &'0';
r<='0'& a(15 downto 1) ;

        with operation select 
             result<=a+b when "000",
             a-b when "001",
             a or b when "101",
             a xor b when "111",
            l when "010",
            r when "011",
            a+b when others;
           
             
       
end Behavioral;
