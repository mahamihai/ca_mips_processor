----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/20/2017 06:03:16 AM
-- Design Name: 
-- Module Name: bauder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bauder is
Port(clk:in std_logic;
baud_rate:out std_logic);
end bauder;

architecture Behavioral of bauder is
signal count:std_logic_vector(15 downto 0);
begin
process(clk)
begin
    if(rising_edge(clk))
        then 
       
        if(count=10416)
            then
            baud_rate<='1';
            count<=x"0000";
            else
            baud_rate<='0';
             count<=count+'1';
            end if;
            end if;
            end process ;

end Behavioral;
