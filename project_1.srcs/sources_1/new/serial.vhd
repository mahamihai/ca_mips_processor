----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/20/2017 06:26:42 AM
-- Design Name: 
-- Module Name: tester - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity serial is
Port(
            tx:out std_logic;
            rx:in std_logic;
            clk : in STD_LOGIC;
         start:in std_logic;
          
          tx_data:in std_logic_vector(7 downto 0));
end serial;

architecture Behavioral of serial is
component bauder is
Port(clk:in std_logic;
baud_rate:out std_logic);
end component;
component TM is
Port(
clk:in std_logic;
tx_data:in std_logic_vector(7 downto 0);
tx_en:in std_logic;
rst:in std_logic;
baud_en:in std_logic;
tx:out std_logic;
tx_ready:out std_logic);

end component;
component debouncer is
Port(
clk:in std_logic;
trigger:in std_logic;
debounced:out std_logic);
end component;
signal baud_en:std_logic;


signal tx_en:std_logic;
signal tx_rdy:std_logic;
signal rst:std_logic;
signal count:std_logic;
signal q:std_logic;
signal tx1:std_logic;
begin
C3:debouncer port map(
clk,
start,
count);

C1:bauder port map (
clk,
baud_en);
C2:TM port map (
clk,
tx_data,
q,
rst,
baud_en,
tx,
tx_rdy);
process(clk)
    begin
        if(rising_edge(clk)) then
            if(count='1') then
         q<='1';
        else
            if(baud_en='1') then
            q<='0';
            else
           end if;
       end if;
        end if;
      --  end if;
    end process;



tx<=tx1;
tx_en<=q;

rst<='0';
end Behavioral;
