

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 use ieee.std_logic_1164.all;  
use ieee.numeric_std.all; 
entity main is

    Port (  tx:out std_logic;
            rx:in std_logic;
            clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (4 downto 0);
           sw : in STD_LOGIC_VECTOR (15 downto 0);
           led : out STD_LOGIC_VECTOR (15 downto 0);
           an : out STD_LOGIC_VECTOR (3 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0));
end main;

architecture Behavioral of main is

component PC is
--  Port ( );
Port(
clk:in std_logic;
branch:in std_logic;
jump:in std_logic;
branch_a:in std_logic_vector(15 downto 0);
jump_a:in std_logic_vector(15 downto 0);
old_a:in std_logic_vector(15 downto 0);
new_a:out std_logic_vector(15 downto 0);
instr:out std_logic_vector(15 downto 0);
pc_1:out std_logic_vector(15 downto 0)
);
end component;
component debouncer is
Port(
clk:in std_logic;
trigger:in std_logic;
debounced:out std_logic);
end component;

component serial is
Port(
            tx:out std_logic;
           rx:in std_logic;
           clk : in STD_LOGIC;
        start:in std_logic;
         tx_data:in std_logic_vector(7 downto 0);
         tx_data2:in std_logic_vector(15 downto 0));
end component;

signal counter:std_logic_vector(15 downto 0):="0000000000000000";
signal count:std_logic;
signal number:std_logic_vector(15 downto 0):="0000000000000000";
signal num:std_logic_vector(15 downto 0):="0000000000000000";

signal digit:std_logic_vector(3 downto 0);
signal sel:std_logic_vector(1 downto 0);
signal ram_enable:std_logic;
type mem is array (0 to 255) of std_logic_vector(15 downto 0);
signal rd1 : std_logic_vector (15 downto 0);
signal rd2 :  std_logic_vector (15 downto 0);
signal fr_enable:std_logic;
signal wd:std_logic_vector(15 downto 0);
signal ram_out:std_logic_vector(15 downto 0);
signal reset:std_logic;
signal shifted_ram:std_logic_vector(15 downto 0);
signal branch_a:std_logic_vector(15 downto 0):=x"0003";
signal jump_a:std_logic_vector(15 downto 0):=x"0006";
signal new_a:std_logic_vector(15 downto 0);
signal sa:std_logic;
signal func:std_logic_vector(2 downto 0);
signal reg_write:std_logic;
signal ext_op:std_logic;
signal reg_dest:std_logic;
signal instr:std_logic_vector(15 downto 0);
signal ext_imm:std_logic_vector(15 downto 0);
signal branch:std_logic;
signal alu_src:std_logic;
signal jump:std_logic;
signal alu_op:std_logic_vector(2 downto 0);
signal mem_write:std_logic;
signal mem_to_reg:std_logic;
signal pc_1:std_logic_vector(15 downto 0);
signal alu_src_vector:std_logic_vector(15 downto 0);
signal alu_execute:std_logic_vector(2 downto 0);
signal zf:std_logic;
signal alu_result:std_logic_vector(15 downto 0);
signal transmit:std_logic;
signal data:std_logic_vector(7 downto 0);
type piped is array(0 to 20) of std_logic_vector(15 downto 0);
signal ifid:piped;
signal idex:piped;
signal exmem:piped;
signal memwb:piped;
signal zero0:std_logic;
signal zero1:std_logic;
signal branch_zero:std_logic;
signal alu_result0:std_logic_vector(15 downto 0);
signal alu_result1:std_logic_vector(15 downto 0);
signal alu_result2:std_logic_vector(15 downto 0);

component test_env is
Port(
    clk:in std_logic;
	reg_write:in std_logic;
	instr:in std_logic_vector(15 downto 0 );
	reg_dest:in std_logic;
	ext_op:in std_logic;
	write_data:in std_logic_vector(15 downto 0);
	rd1:out std_logic_vector(15 downto 0);
	rd2:out std_logic_vector(15 downto 0);
	ext_imm:out std_logic_vector(15 downto 0 );
	funct:out std_logic_vector(2 downto 0);
	sa:out std_logic
	);
end component;

component decoder is
Port(
clk:in std_logic;
instr:in std_logic_vector(2 downto 0);
reg_dest:inout std_logic;
ext_op:inout std_logic;
alu_src:inout std_logic;
branch:inout std_logic;
jump:inout std_logic;
alu_op:inout std_logic_vector(2 downto 0);
mem_write:inout std_logic;
mem_to_reg:inout std_logic;
reg_write:inout std_logic
);


end component;

component ALU is
--  Port ( );
Port(

a:in std_logic_vector(15 downto 0);
b:in std_logic_vector(15 downto 0);
operation:in std_logic_vector(2 downto 0);
zf:out std_logic;
result:inout std_logic_vector(15 downto 0)
);
end component;

component RAM is
--  Port ( );
Port(
clk:in std_logic;
ram_enable:in std_logic;

ram_data:in std_logic_vector(15 downto 0);
num:in std_logic_vector(15 downto 0 );
ram_out:out std_logic_vector(15 downto 0)

);
end component;
begin
C20:serial port map 
(tx,
rx,
clk,
transmit,
data,
wd
);




  
 
C5:RAM port map(clk,
ram_enable,
exmem(2),
alu_result,
ram_out);
C2:debouncer port map(
clk,
btn(4),
count);

C3:debouncer port map(
clk,
btn(3),
fr_enable
);
c7:debouncer port map(
clk,
btn(2),
reset);
C4:debouncer port map 
(
clk,
btn(1),
ram_enable
);

C10:test_env port map
(
count,
reg_write,
ifid(1),
reg_dest,
ext_op,
wd,
rd1,
rd2,
ext_imm,
func,
sa);


c12:ALU port map
(idex(1),
alu_src_vector,
alu_execute,
zf,
alu_result0
);
C11:decoder port map
(
count,
ifid(1)(15 downto 13),
reg_dest,
ext_op,
alu_src,
branch,
jump,
alu_op,
mem_write,
mem_to_reg,
reg_write
);


PCU:PC port map(clk,
 branch_zero,
  jump,
  branch_a,
  jump_a,
  num,
  new_a,
  instr,
  pc_1
  );
 transmit<='1' when instr=x"0830" else '0';
 
  led(7 downto 0)<=wd(7 downto 0);
branch_zero<=zero1 and branch;

--branch_a<=
--led(0)<=reg_write;

--led(1)<=mem_to_reg;
--led(2)<=mem_write;
--led(3)<=alu_op(0);
--led(4)<=alu_op(1);
--led(5)<=alu_op(2);
--led(6)<=jump;
--led(7)<=branch;
--led(8)<=alu_src;
--led(9)<=ext_op;
--led(10)<=reg_dest;
--led(13 downto 11)<=alu_op;
--led(15)<='1';

zero0<=zf;


   with sw(15 downto 13) select
    number<=instr when "000",
           idex(1) when "001",
            alu_result1  when "011",
            ifid(1) when "010",
           -- alu_src_vector
           idex(0) when "100",
            wd when "101",
            jump_a when "110",
            ifid(0) when "111";
  --------------------------------------------------------
 process(clk)
 begin
    if(rising_edge(count)) then
   ------ MEM/WB
               alu_result1<=alu_result;

   ------EX/MEM
            alu_result<=alu_result0;
            zero1<=zero0;
            exmem(2)<=idex(2);
     --------IDEX
           idex(0)<=ifid(0);
           idex(1)<=rd1;
           idex(2)<=rd2;
           idex(3)<=ext_imm;
          
    -------IFID
        ifid(0)<=pc_1;
        ifid(1)<=instr;
   
        
    end if;
 end process;         


jump_a<="000"&ifid(1)(12 downto 0) ;
wd<= ram_out when mem_to_reg='1' else alu_result1;
alu_execute<=func when conv_integer(instr(15 downto 13))=0 else alu_op;
alu_src_vector<=idex(2) when alu_src='1' else idex(3);
process(clk)
begin

if(rising_edge(clk))
then
    shifted_ram<=ram_out(13 downto 0) & "00";
    counter<=counter+'1';
 
 end if;
end process;
--if(btn(4)='1' and q2)

 ---------------------------------------------------------------------------------
 
-- with sw(15 downto 13) select
--     number<=num when "111",
--     rom(conv_integer(num(7 downto 0))) when "000",
--     rd1 when "001",
--     rd2 when "010",
--     wd when "011",
     
--    ram_out  when "100",
--    shifted_ram when "101",
    
--     x"FFFF" when others;
   --x"FFFFF" when others;

    
     
   ---------------------------------------------------------
 process(clk)
    begin
    if(rising_edge(clk)) then
        if(count='1')
            then
                num<=new_a;
                
             end if;
           if(reset='1')
                     then
                
                     num<=x"0000";
                 end if;
             end if;
          end process;
        

sel<=counter(15 downto 14);
process(clk)
   
    begin
     if(rising_edge(clk)) then
        case sel is
            when "00"=>an<="1110";
               digit<=number(3 downto 0);
             when "01"=>an<="1101";
              digit<=number(7 downto 4);
              when "10"=>an<="1011";
               
               digit<=number(11 downto 8);
               
               when "11"=>an<="0111";
                digit<=number(15 downto 12);
                
    end case;
      end if;       
        
end process;

process(sw(15 downto 12))
begin
--IMPLEMENT ALU
end process;
with digit SELect
   cat<= "1111001" when "0001",   --1
         "0100100" when "0010",   --2
         "0110000" when "0011",   --3
         "0011001" when "0100",   --4
         "0010010" when "0101",   --5
         "0000010" when "0110",   --6
         "1111000" when "0111",   --7
         "0000000" when "1000",   --8
         "0010000" when "1001",   --9
         "0001000" when "1010",   --A
         "0000011" when "1011",   --b
         "1000110" when "1100",   --C
         "0100001" when "1101",   --d
         "0000110" when "1110",   --E
         "0001110" when "1111",   --F
         "1000000" when others;   --0



end Behavioral;
