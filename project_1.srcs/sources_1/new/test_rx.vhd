----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2017 08:36:03 AM
-- Design Name: 
-- Module Name: test_rx - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test_rx is
 Port(tx:out std_logic;
            rx:in std_logic;
            clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (4 downto 0);
           sw : in STD_LOGIC_VECTOR (15 downto 0);
           led : out STD_LOGIC_VECTOR (15 downto 0);
           an : out STD_LOGIC_VECTOR (3 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0));
end test_rx;

architecture Behavioral of test_rx is
component rx is
    Port(
    clk:in std_logic;
    rx_rdy:out std_logic;
    rx_data:out std_logic_vector(7 downto 0);
    rst:in std_logic;
    rx:in std_logic);
end component;
signal rx_rdy:std_logic;
signal data:std_logic_vector(7 downto 0);
signal rst:std_logic;
begin
c3:rx port map (
clk,
rx_rdy,
data,
rst,
rx_in);

process(clk)
begin
    if(rising_edge(clk))then
        led(7 downto 0)<=data;
    end if;
 end process;




end Behavioral;
