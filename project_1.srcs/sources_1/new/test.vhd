----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2017 05:41:19 AM
-- Design Name: 
-- Module Name: test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test is
  Port (  tx:out std_logic;
          rx:in std_logic;
          clk : in STD_LOGIC;
         btn : in STD_LOGIC_VECTOR (4 downto 0);
         sw : in STD_LOGIC_VECTOR (15 downto 0);
         led : out STD_LOGIC_VECTOR (15 downto 0);
         an : out STD_LOGIC_VECTOR (3 downto 0);
         cat : out STD_LOGIC_VECTOR (6 downto 0));
end test;

architecture Behavioral of test is
component serial is
Port(
            tx:out std_logic;
            rx:in std_logic;
            clk : in STD_LOGIC;
            start:in std_logic;
          
          tx_data:in std_logic_vector(7 downto 0));
end component;
signal transmit:std_logic;
signal data:std_logic_vector(7 downto 0);
begin
C20:serial port map 
(tx,
rx,
clk,
transmit,
data
);
transmit<='1';
data<=sw(7 downto 0);
led(7 downto 0)<=data;

end Behavioral;
